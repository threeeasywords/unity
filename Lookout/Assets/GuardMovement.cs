﻿using UnityEngine;
using System.Collections;

public class GuardMovement : MonoBehaviour {

	Transform Graphic;
	Transform Light;
	Rigidbody2D rb;
	Transform t;
	Animator a;
	float speed = 5;

	// Use this for initialization
	void Start () {
		rb = GetComponent<Rigidbody2D> ();
		t = transform;
		Graphic = t.GetChild (0);
		Light = t.GetChild (1);
		a = Graphic.GetComponent<Animator> ();
	}

	// Update is called once per frame
	void Update () {
		Vector3 dir = Vector3.zero;

		if (Input.GetKey(KeyCode.W)) { 
			dir += Vector3.up;
		}
		if (Input.GetKey(KeyCode.S)) {
			dir += Vector3.down;
		}
		if (Input.GetKey(KeyCode.D)) {
			dir += Vector3.right;
		}
		if (Input.GetKey(KeyCode.A)) {
			dir += Vector3.left;
		}
		if (dir.x > 0) {
			Graphic.eulerAngles = new Vector3 (0, 180, 0);
		} else if (dir.x < 0) {
			Graphic.eulerAngles = new Vector3 (0, 0, 0);
		}

		if (dir == Vector3.zero) {
			a.speed = 0;
		} else {
			a.speed = 1;
		}

		if (dir == Vector3.down) {
			a.SetInteger ("Direction", 0);
			Light.eulerAngles = new Vector3 (0, 0, 180);
		} else if (dir == Vector3.left + Vector3.down) {
			a.SetInteger ("Direction", 1);
			Light.eulerAngles = new Vector3 (0, 0, 135);
		} else if (dir == Vector3.right + Vector3.down) {
			a.SetInteger ("Direction", 1);
			Light.eulerAngles = new Vector3 (0, 0, -135);
		} else if (dir == Vector3.left) {
			a.SetInteger ("Direction", 2);
			Light.eulerAngles = new Vector3 (0, 0, 90);
		}  else if (dir == Vector3.right) {
			a.SetInteger ("Direction", 2);
			Light.eulerAngles = new Vector3 (0, 0, -90);
		} else if (dir == Vector3.left + Vector3.up) {
			a.SetInteger ("Direction", 3);
			Light.eulerAngles = new Vector3 (0, 0, 45);
		} else if (dir == Vector3.right + Vector3.up) {
			a.SetInteger ("Direction", 3);
			Light.eulerAngles = new Vector3 (0, 0, -45);
		} else if (dir == Vector3.up) {
			a.SetInteger ("Direction", 4);
			Light.eulerAngles = new Vector3 (0, 0, 0);
		}

		rb.MovePosition (t.position + dir * speed * Time.deltaTime);
	}
}

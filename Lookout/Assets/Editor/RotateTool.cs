﻿using UnityEngine;
using UnityEditor;

using Rotorz.Tile;
using Rotorz.Tile.Editor;
using System.Collections.Generic;

[InitializeOnLoad]
public class RotateTool : ToolBase {

	//List<Transform> Events = new List<Transform>();
	Transform foundEvent;
	TileData movedData;
	TileIndex movedIndex;

	bool isNew = false;
	List<KeyCode> currentKeys = new List<KeyCode>();
	bool rightButton = false;

	Transform selectedEvent;

	static RotateTool() {
		ToolManager.Instance.RegisterTool<RotateTool>();
	}

	public override string Label {
		get { return "Rotate"; }
	}
	public string Name {
		get { return "my.rotate-tool"; }
	}

	public override void OnCheckKeyboardShortcuts() {
		KeyCode keyCode = UnityEngine.Event.current.keyCode;


		if (UnityEngine.Event.current.type == EventType.keyDown) {
			if (keyCode != KeyCode.None) {
				if (!currentKeys.Contains (keyCode)) {
					currentKeys.Add (keyCode);
				}
			}
		} else if (UnityEngine.Event.current.type == EventType.keyUp) {
			if (keyCode != KeyCode.None) {
				currentKeys.Remove (keyCode);
			}
		}
		if (UnityEngine.Event.current.isKey) {
			UnityEngine.Event.current.Use ();
		}
	}

	public override void OnToolInactive(ToolEvent e, IToolContext context) {
		
	}

	public override void OnToolOptionsGUI() {
		
		GUILayout.BeginHorizontal();
		GUILayout.FlexibleSpace ();
		GUILayout.Label("Hold E to drag tiles with events.");
		GUILayout.FlexibleSpace ();
		GUILayout.EndHorizontal();
	}

	Transform FindEvent(Vector2 pos) {
		GameObject[] events = GameObject.FindGameObjectsWithTag("Event");
		Transform foundEvent = null;
		for (int i = 0; i < events.Length; i++) {
			Vector2 eventPos = (Vector2)events [i].transform.position;
			if (eventPos == pos || Vector2.Distance(eventPos,(Vector2)pos) <= .5f) {
				foundEvent = events[i].transform;
				break;
			}
		}
		return foundEvent;
	}

	bool AngletoFlag(int flagNum, int angle) {
		angle = (int)Mathf.DeltaAngle (0, angle);
		if (angle == 180) {
			return true;
		} else if (angle == 0) {
			return false;
		}
		if (angle == 90) {
			if (flagNum == 2) {
				return true;
			} else {
				return false;
			}
		} else if (angle == -90) {
			if (flagNum == 2) {
				return false;
			} else {
				return true;
			}
		}
		return false;
	}

	public override void OnTool(ToolEvent e, IToolContext context) {
		
		if (ToolManager.Instance.CurrentTool != this)
			return;

		bool keyE = false;
		if (currentKeys.Contains (KeyCode.E)) {
			keyE = true;
		}

		Vector2 pos = context.TileSystem.WorldPositionFromTileIndex (e.tileIndex);

		TileData data = context.TileSystem.GetTile (e.tileIndex);

		if (e.type == EventType.MouseDown)
			rightButton = e.rightButton;
		
	

		if (rightButton) {
			context.TileSystem.EraseTile (e.tileIndex);
		}
		else if (!rightButton) {
			if (data == null) {
				if (!ToolUtility.SelectedBrush.PerformsAutomaticOrientation)
				ToolUtility.SelectedBrush.Paint (context.TileSystem, e.tileIndex);
				return;
			}
			if (data.gameObject && e.type == EventType.mouseDown) {
					int angle = (int)data.gameObject.transform.eulerAngles.z;
					angle -= 90;
					data.gameObject.transform.eulerAngles = new Vector3 (0, 0, angle);

					data.SetUserFlag (16, AngletoFlag (2, angle));
					data.SetUserFlag (15, AngletoFlag (1, angle));
				}
		}
/*

		if (e.type == EventType.MouseUp && movedIndex == e.tileIndex) {
			if (!e.previousRightButton) {
				if (!isNew) {
					Selection.activeGameObject = foundEvent.gameObject;
				}
			}
		}

		if (e.previousRightButton) {
			if (keyE || data != null && data.GetUserFlag(1)) {
				context.TileSystem.EraseTile (e.tileIndex);
			}
			Transform t = FindEvent (pos);
			if (t) {
				GameObject.DestroyImmediate (t.gameObject);
			}
		}

		if (e.type == EventType.mouseUp && movedIndex != e.tileIndex) {
			
			if (keyE) {
				if (movedData != null) {
					Brush b = movedData.brush;
					context.TileSystem.EraseTile (movedIndex);
					context.TileSystem.RefreshSurroundingTiles (movedIndex);
					b.Paint (context.TileSystem, (e.tileIndex));
					context.TileSystem.RefreshSurroundingTiles (e.tileIndex);
				}
			}

			if (foundEvent != null && FindEvent(pos) == null) {
				foundEvent.transform.position = pos;
			}
		}

		if (e.type == EventType.mouseUp) {
			foundEvent = null;
		}

*/
		// Do something magical!
	}


}
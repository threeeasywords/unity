﻿using UnityEngine;
using UnityEditor;

using Rotorz.Tile;
using Rotorz.Tile.Editor;
using System.Collections.Generic;

[InitializeOnLoad]
static class CustomSettings {

	static bool isR = false;
	static Brush toAutoRotate;
	static Vector2 mousePosition;

	static CustomSettings() {
		PaintingUtility.TilePainted += TilePaint;   // subscribe to vanilla C# event
		PaintingUtility.WillEraseTile += TileErase;   // subscribe to vanilla C# event
		EditorApplication.update += Update;
		SceneView.onSceneGUIDelegate += OnSceneGUI;
	}

	private static void OnSceneGUI(SceneView sceneView) {
		int controlID = GUIUtility.GetControlID(FocusType.Passive);

		mousePosition = UnityEngine.Event.current.mousePosition;

		if (UnityEngine.Event.current.keyCode == KeyCode.R)
		{
			if (UnityEngine.Event.current.GetTypeForControl (controlID) == EventType.keyDown) {
				if (UnityEngine.Event.current.keyCode == KeyCode.R)
					isR = true;
			} else if (UnityEngine.Event.current.GetTypeForControl (controlID) == EventType.keyUp) {
				if (UnityEngine.Event.current.keyCode == KeyCode.R)
					isR = false;
			}
			// Causes repaint & accepts event has been handled
			UnityEngine.Event.current.Use();
		}
	}

	static void Update () {
	}
		

	static Brush BrushByName(string name) {
		for (int i = 0; i < BrushDatabase.Instance.BrushRecords.Count; i++) {
			Brush b = BrushDatabase.Instance.BrushRecords [i].Brush;
			if (b.name == name) {
				return b;
			}
		}
		return null;
	}

	private static void TileErase(WillEraseTileEventArgs args) {

	}

	private static void TilePaint(TilePaintedEventArgs args) {
		// Tile might not have a game object attached!
		if (args.TileData.HasGameObject && args.TileData.gameObject.GetComponent<BoxCollider2D>()) {
			args.TileData.gameObject.GetComponent<BoxCollider2D> ().size = new Vector2 (1.01f, 1.01f);
		}

		if (args.GameObject != null) {
			if (args.TileData.GetUserFlag (16)) {
				if (args.TileData.GetUserFlag (15)) {
					args.GameObject.transform.eulerAngles = new Vector3 (0, 0, 180);
				} else {
					args.GameObject.transform.eulerAngles = new Vector3 (0, 0, 90);

				}
			} else {
				if (args.TileData.GetUserFlag (15)) {
					args.GameObject.transform.eulerAngles = new Vector3 (0, 0, -90);
				} else {
					args.GameObject.transform.eulerAngles = new Vector3 (0, 0, 0);

				}
			}

		}
	}

}
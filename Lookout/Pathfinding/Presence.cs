﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Presence : MonoBehaviour {
	
	public LayerMask pathMask;
	public LayerMask playerMask;
	Transform t;

	// Use this for initialization
	void Start () {
		t = transform;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			Present ();
		}
	}
	
	void Present () {
		RaycastHit2D[] playerHits =  Physics2D.CircleCastAll (transform.position, 200, Vector3.zero, 0, playerMask);
		if (!CheckCircle (playerHits)) {
			RaycastHit2D[] pathHits =  Physics2D.CircleCastAll (transform.position, 200, Vector3.zero, 0, pathMask);
			CheckCircle (pathHits);
		}
	}

	bool CheckCircle(RaycastHit2D[] circleHits) {
		List<Transform> paths = new List<Transform>();
		paths.Add(transform);
		PathData data = new PathData (paths, 0, Time.time);
		for (int i = 0; i < circleHits.Length; i++) {
			Transform h = circleHits [i].transform;
			if (h != t) {
				Vector3 dir = h.position - t.position;
				dir = dir / dir.magnitude;
				RaycastHit2D[] hits = Physics2D.RaycastAll (t.position, dir);
				for (int j = 0; j < hits.Length; j++) {
					RaycastHit2D hit = hits [j];
					if (hit.transform.tag != "NPC" && hit.transform.tag != "Path" &&
					    hit.transform.tag != "Player") {
						break;
					} else if (hit.transform == h) {
						if (h.tag == "Path") {
							//break;
							h.GetComponent<PathManager> ().Follow (data);
							//return false;
							break;
						}
					}
				}
			}
		}
		return false;
	}
}

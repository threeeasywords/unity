﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PathManager : MonoBehaviour {

	public List<Transform> Paths;
	public LayerMask pathMask;
	public List<Path> subPaths = new List<Path>();
	Transform t;
	
	// Use this for initialization
	void Start () {
		t = transform;
		AddPath ();
		FindPaths ();
	}
	
	Path AddPath () {
		Path p = t.gameObject.AddComponent<Path> ();
		p.pathMask = pathMask;
		subPaths.Add (p);
		return p;
	}
	
	void FindPaths () {
		Paths = new List<Transform> ();
		RaycastHit2D[] circleHits = Physics2D.CircleCastAll (t.position, 200, Vector3.zero, 0, pathMask);
		for (int i = 0; i < circleHits.Length; i++) {
			Transform h = circleHits [i].transform;
			if (h != t && !Paths.Contains (h)) {
				if (h.tag == "Path") {
					Vector3 dir = h.position - t.position;
					dir = dir / dir.magnitude;
					RaycastHit2D hit = Physics2D.Raycast (t.position, dir);
					if (hit.collider && hit.transform == h) {
						Paths.Add (h);
						if (!h.GetComponent<PathManager> ().Paths.Contains (t)) {
							h.GetComponent<PathManager> ().Paths.Add (t);
						}
					}
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
		

	public void Follow (PathData data) {
		for (int i = 0; i < subPaths.Count; i++) {
			if (subPaths[i].currentData.Paths.Count == 0 ||
				subPaths[i].currentData.Paths.Count > 0 && subPaths[i].currentData.Paths[0] == data.Paths[0]) {
				subPaths [i].Follow (data);
				return;
			}
		}
		AddPath ().Follow (data);
	}
}

﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Path : MonoBehaviour {
	
	public PathData currentData;
	public LayerMask pathMask;
	public LayerMask playerMask;
	PathManager manager;
	Transform t;
	Transform p;
	
	// Use this for initialization
	void Awake () {
		currentData = new PathData ();
		t = transform;
		p = GameObject.FindGameObjectWithTag ("Player").transform;
		manager = GetComponent<PathManager> ();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Reset () {
		currentData = new PathData ();
	}
	
	public void Follow (PathData data) {
		data.Distance += Vector2.Distance (t.position, data.Paths[data.Paths.Count-1].position) - 1;

		if (data.Distance <= currentData.Distance || data.Time > currentData.Time) {

			CancelInvoke ("Reset");
			Invoke ("Reset", .05f);

			data.Paths.Add (transform);

			currentData = data;

			for (int j = 0; j < manager.Paths.Count; j++) {
				if (!data.Paths.Contains (manager.Paths [j])) {
					manager.Paths [j].GetComponent<PathManager> ().Follow (data);
				}
			}
		}
	}
}

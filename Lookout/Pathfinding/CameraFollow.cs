﻿using UnityEngine;
using System.Collections;

public class CameraFollow : MonoBehaviour {

	Transform p;

	// Use this for initialization
	void Start () {
		p = GameObject.FindGameObjectWithTag ("Player").transform;
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = new Vector3 (p.position.x,
		                                 p.position.y,
		                                 transform.position.z);
	}
}

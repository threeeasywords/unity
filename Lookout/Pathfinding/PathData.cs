﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class PathData {
	
	public List<Transform> Paths = new List<Transform> ();
	public float Distance = 100000;
	public float Time = 0;

	public PathData (List<Transform> paths, float distance, float time) {
		Paths = paths;
		Distance = distance;
		Time = time;
	}

	public PathData() {
		Paths = new List<Transform> ();
		Distance = 100000;
		Time = 0;
	}
}

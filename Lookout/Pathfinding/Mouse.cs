﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using Rotorz.Tile;

public class Mouse : MonoBehaviour {
	
	public LayerMask pathMask;
	public Brush Brush;
	List<Vector3> mousePositions = new List<Vector3> ();
	Transform t;
	Transform p;
	Rigidbody2D rb;
	TileSystem tileSystem; 
	
	// Use this for initialization
	void Start () {
		p = GameObject.FindGameObjectWithTag ("Player").transform;
		rb = p.GetComponent<Rigidbody2D> ();
		t = transform;
		tileSystem = GameObject.FindGameObjectWithTag ("Walls").GetComponent<TileSystem>();
		SetPaths ();
	}

	void SetPaths () {
		for (int row = 0; row < tileSystem.RowCount; ++row) {
			for (int column = 0; column < tileSystem.ColumnCount; ++column) {
				TileData tile = tileSystem.GetTile(row, column);
				// Skip empty tile
				if (!isEmpty(tile)) {
					List<Vector2> corners = Corners(row,column);
					for (int i = 0; i < corners.Count; i++) {
							Brush.Paint(tileSystem,(int)corners[i].x,(int)corners[i].y);
							//corners.Add (new Vector2(row,column));
							//Brush.Paint(tileSystem,row,column);
					}
				}
				
				// Do something with tile!
			}
		}
	}

	List<Vector2> Corners (int row, int column) {
		List<Vector2> corners = new List<Vector2> ();
		List<bool> empties = new List<bool> ();

		for (int y = row -1; y <= row + 1; y++) {
			for (int x = column -1; x <= column + 1; x++) {
				bool empty = isEmpty(tileSystem.GetTile(y,x));
				empties.Add (empty);
			}
		}

		/* GRID LAYOUT
		 * 	---columm---
		 * r	012
		 * o	345
		 * w	678
		 */

		if (empties[0] && empties[1] && empties[3] && empties[2]) {
			corners = AddCornerEmpty (corners, row-1, column-2);
		}
		if (empties[0] && empties[1] && empties[3] && empties[6]) {
			corners = AddCornerEmpty (corners, row-1, column-2);
		}

		if (empties[1] && empties[2] && empties [5]  && empties[8]) {
			corners = AddCornerEmpty (corners, row-1, column+1);
		}
		if (empties[1] && empties[2] && empties [5]  && empties[0]) {
			corners = AddCornerEmpty (corners, row-1, column+1);
		}

		if (empties[2] && empties[5] && empties[8] && empties[7]) {
			corners = AddCornerEmpty (corners, row+2, column+1);
		}

		if (empties[6] && empties[5] && empties[8] && empties[7]) {
			corners = AddCornerEmpty (corners, row+2, column+1);
		}

		if (empties[0] && empties[3] && empties[6] && empties[7]) {
			corners = AddCornerEmpty (corners, row+2, column-2);
		}
		if (empties[8] && empties[3] && empties[6] && empties[7]) {
			corners = AddCornerEmpty (corners, row+2, column-2);
		}

		if (empties[1] && empties[2] && empties[3]  && empties[5] &&
		    empties[6] &&  empties[7] && empties[8]) {
			corners = AddCornerEmpty (corners, row+2, column+1);
		}
		
		return corners;
	}

	List<Vector2> AddCornerEmpty (List<Vector2> corners, int row, int column) {
		if (!isEmpty (tileSystem.GetTile (row, column)))
			return corners;

		if (!isEmpty (tileSystem.GetTile (row, column+1)))
			return corners;
		
		if (!isEmpty (tileSystem.GetTile (row-1, column+1)))
			return corners;

		
		if (!isEmpty (tileSystem.GetTile (row-1, column)))
			return corners;

	//	Debug.Log ((row +  1).ToString () + "-" +  (column + 2).ToString ());
			
		corners.Add (new Vector2 (row, column));

		return corners;
	}

	bool isEmpty(TileData tile) {
		if (tile == null || tile.Name != "Empty")
			return true;

		return false;
	}

	void Update () {
		Vector3 mousePos = Input.mousePosition;

		if (Input.GetMouseButton (0)) {
			mousePositions.Add (mousePos);
		}

		if (Input.GetMouseButtonUp (0) && mousePositions.Count > 0) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			TileIndex ti = tileSystem.ClosestTileIndexFromRay(ray);

			float range = Vector2.Distance (Camera.main.ScreenToWorldPoint(mousePositions[0]), Camera.main.ScreenToWorldPoint(mousePositions[mousePositions.Count-1]));
			if (range > 1f && range < 100) {
				Vector3 dir = Camera.main.ScreenToWorldPoint(mousePositions[mousePositions.Count-1])-Camera.main.ScreenToWorldPoint(mousePositions[0]);
				rb.AddForce (dir*20, ForceMode2D.Impulse);
				Vector3 lastPos = lastPosition ();
				if (lastPos != Vector3.zero) t.position = lastPos;
			}
			//else if (!isEmpty (tileSystem.GetTile (ti))) {
			//	tileSystem.SetTile (ti.row, ti.column, null);
			//}
			else {
				t.position = Camera.main.ScreenToWorldPoint(mousePositions[mousePositions.Count-1]);
				Present ();
			}
			mousePositions.Clear ();
		}
	}

	Vector3 lastPosition () {
		Vector3 lastPos = Vector3.zero;
		for (int i = 0; i < mousePositions.Count; i++) {
			Vector3 dir = p.position-mousePositions[i];
			dir = dir/dir.magnitude;
			RaycastHit2D hit = Physics2D.Raycast (mousePositions[i], dir);
			if (hit && hit.transform == p) {
				lastPos = mousePositions[i];
			}
		}
		return lastPos;

	}

	void Present () {
		RaycastHit2D[] pathHits =  Physics2D.CircleCastAll (transform.position, 200, Vector3.zero, 0, pathMask);
		CheckCircle (pathHits);
	}

	void CheckCircle(RaycastHit2D[] circleHits) {
		List<Transform> paths = new List<Transform>();
		paths.Add(transform);
		PathData data = new PathData (paths,0, Time.time);
		for (int i = 0; i < circleHits.Length; i++) {
			Transform h = circleHits[i].transform;
			if (h.tag == "Path") {
				Vector3 dir = h.position-t.position;
				dir = dir/dir.magnitude;
				RaycastHit2D hit = Physics2D.Raycast (t.position, dir);
				if (hit.collider && hit.transform == h) {
					h.GetComponent<PathManager>().Follow (data);
				}
			}
		}
	}
}

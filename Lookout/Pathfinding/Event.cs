using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class Event : MonoBehaviour {

	public Transform[] Targets;
	public Transform OnTrigger;
	public Transform OffTrigger;
	public float Speed = 1;
	public bool Loop = false;
	public string[] Route;
	public bool Flip = true;
	public float CloseDist = 0;
	Transform target;
	string followTag = "";
	string oldRoute = "";
	int followMove = -1;
	public PathData currentData;

	bool triggered = false;
	int currentMove = 0;
	Vector3[] checkPoints;
	Vector3 checkPoint;
	Collider2D c;
	Animator a;

	string targetNum = "";

	string[] moves;
	int index = 0;
	bool nextMove = true;
	public string[] Switches;

	LayerMask pathMask = (1<<8);

	// Use this for initialization
	void Start () {
		c = GetComponent<Collider2D> ();
		a = GetComponent<Animator> ();
		target = transform;
		ResetFollow ();
	}
	
	// Update is called once per frame
	void Update () {
		if (OnTrigger && !triggered)
			return;

		for (int i = 0; i < Switches.Length; i++) {
			string[] values = Switches[i].Split(new[] { ", " }, System.StringSplitOptions.None);
			if (PlayerPrefs.GetString (values[0].ToUpper ()) == "") PlayerPrefs.SetString (values[0].ToUpper (), "FALSE");
			if (PlayerPrefs.GetString (values[0].ToUpper ()) != values[1].ToUpper ()) {
				return;
			}
		}

		Move ();
		if (followMove != -1) {
			Follow (target);
		}
	}

	void OnTriggerStay2D (Collider2D collider) {
		if (collider.transform == OnTrigger) {
			triggered = true;
		}
	}

	void OnTriggerExit2D (Collider2D collider) {
		if (collider.transform == OffTrigger) {
			triggered = false;
		}
	}

	void Move () {
		if (Route.Length == 0)
			return;

		if (currentMove >= Route.Length) {
			if (!Loop) return;
			currentMove = 0;
			triggered = false;
		}

		Route [currentMove] = Route [currentMove].ToUpper ();

		if (nextMove) {
			target = transform;
			if (Route[currentMove].Contains("FOLLOW")) {
				if (!Route[currentMove].Contains ("MOVE TO ")) oldRoute = Route[currentMove];
			}
			else {
				oldRoute = "";
				followTag = "";
				followMove = -1;
			}

			moves = Route [currentMove].Split (new[] { " && " }, System.StringSplitOptions.None);
			checkPoints = new Vector3[moves.Length];
			for (int i = 0; i < checkPoints.Length; i++) {
				checkPoints[i] = new Vector3(-100,-100,-100);
			}

			nextMove = false;
		}

		if (Route [currentMove].ToUpper ().Contains ("FOLLOW"))
			followMove = currentMove;

		for (int i = 0; i < moves.Length; i++) {

			if (moves[i].ToUpper().Contains("FOLLOW")) followTag = moves[i].ToUpper().Replace ("FOLLOW ", null);
			
			index = i;
			string route = moves[i];
			if (route.Contains ("TARGET")) {
				targetNum = "";
				for (int j = route.IndexOf("TARGET")+7; j < route.Length; j++) {
					if (route[j] == ':') break;
					targetNum += route[j];
				}

				target = Targets[int.Parse (targetNum)];
				Go (target, moves[i].Replace ("TARGET " + targetNum + ": ", null));
			}
			else {
				Go (target, moves[i]);
			}
		}
	}
	
	void Go (Transform T, string route) {
		int lastMove = currentMove;
		if (route.Contains ("SPEAK")) {
			string text = route.Replace ("SPEAK ", null);
			T.SendMessage ("Speak", text);
			Next ();
			return;
		}

		if (checkPoints[index] == new Vector3 (-100, -100, -100)) {
			if (route.Contains ("MOVE")) {
				checkPoints[index] = T.position;
			}
			if (route.Contains ("TURN"))
				checkPoints[index] = T.localEulerAngles;
			if (route.Contains ("RESIZE")) {
				checkPoints[index] = new Vector3(T.GetComponent<Camera>().orthographicSize,
				                                 0,0);
			}
		}
		checkPoint = checkPoints [index];

		if (route.Contains ("FOLLOW")) {
			Next ();
		}

		if (route.Contains ("SHOOT ON")) {
			T.gameObject.SendMessage ("Shoot", true);
			Next ();
		}
		if (route.Contains ("SHOOT OFF")) {
			T.gameObject.SendMessage ("Shoot", false);
			Next ();
		}

		if (route.Contains ("SWITCH")) {
			string[] values = route.Replace ("SWITCH ", null).Split(' ');
			PlayerPrefs.SetString (values[0].ToUpper (), values[1].ToUpper ());
			Next ();
		}

		if (route.Contains ("CHECKPOINT")) {
			if (!route.Contains ("AT")) {
				T.gameObject.SendMessage ("Checkpoint", Application.loadedLevelName + ": " + transform.position.ToString (), SendMessageOptions.DontRequireReceiver);
			}
			else {
				T.gameObject.SendMessage ("Checkpoint", route.Replace ("CHECKPOINT AT ", null), SendMessageOptions.DontRequireReceiver);
			}
			Next ();
		}

		if (route.Contains ("PLACE AT ")) {
			string[] positions = route.Replace ("PLACE AT ", null).Replace ("(", null).Replace (")", null).Split (',');
			Vector3 position;
			if (positions.Length > 2) {
				position = new Vector3(float.Parse (positions[0]), float.Parse (positions[1]),float.Parse (positions[2]));
			}
			else {
				position = new Vector3(float.Parse (positions[0]), float.Parse (positions[1]),T.position.z);
			}
			T.gameObject.transform.position = position;
			Next ();
		}

		/*
		if (route.Contains ("FADE")) {
			Fade fade = GameObject.FindGameObjectWithTag("Fader").GetComponent<Fade>();
			if (route.Contains ("FADE TO")) {
				fade.Level = route.Replace ("FADE TO ", null);
			}
			fade.fadeOut = true;
			Next ();
		}
		*/

		if (route.Contains ("KEY")) {
			if (route.Contains ("A"))  T.eulerAngles = new Vector3(0,0,0);
			if (route.Contains ("D"))  T.eulerAngles = new Vector3(0,180,0);
			T.SendMessage ("Key", route.Replace ("KEY ", null));
			Next ();
		}

		if (route.Contains ("MOVE TO")) {
			string[] positions = route.Replace ("MOVE TO ", null).Replace ("(", null).Replace (")", null).Split (',');
			Vector3 position = new Vector3(float.Parse (positions[0]), float.Parse (positions[1]),T.position.z);
			if (Vector2.Distance (T.position,position) <= CloseDist) {
				if (followMove == currentMove) {
					if (currentData.Paths.Count > 1) {
						currentData.Paths.RemoveAt (currentData.Paths.Count - 1);
						Vector3 sendPos = currentData.Paths [currentData.Paths.Count - 1].position;
						string newRoute = "MOVE TO " + "(" + sendPos.x + "," + sendPos.y + ")";
						Route [followMove] = "FOLLOW " + followTag + " && " + newRoute;
						Debug.Log ("FOLLOW " + followTag + " && " + newRoute);
					} else {
						ResetFollow ();
					}
					Next ();
				}
			}
			Vector3 direction = position-T.position;
			direction = direction/direction.magnitude;
			if (Flip) {
				if (direction.x > 0) T.localScale = new Vector3(1,1,1);
				if (direction.x < 0) T.localScale = new Vector3(-1,1,1);
			}
			//if (direction != Vector3.zero) {
			//T.GetComponent<Rigidbody2D>().AddForce(direction*250);
			T.GetComponent<Rigidbody2D>().MovePosition (Vector3.MoveTowards (T.position, position, Speed));
			//	T.Translate (direction * Speed * Time.deltaTime);
		//	}
		}

		if (route.Contains ("ORDER TO")) {
			int order = int.Parse (route.Replace ("ORDER TO ", null));
			T.position = new Vector3(T.position.x, T.position.y, order);
			Next ();
		}

		if (route.Contains ("WAIT")) {
			if (route.Contains ("FOR")) {
				if (Input.GetKeyDown ((KeyCode)Enum.Parse(typeof(KeyCode),route.Replace ("WAIT FOR ", null)))) {
					Next ();
				}
			}
			else if (!IsInvoking("EndTime") && !route.Contains ("INFINITY")) {
				float time = float.Parse (route.Replace ("WAIT ", null));
				Invoke ("EndTime", time);
			}
		}

		if (route.Contains ("SPEED =")) {
			Speed = float.Parse (route.Replace ("SPEED = ", null));
			Next ();
		}

		if (route.Contains ("HOLD")) {
			T.gameObject.SendMessage("Hold", true);
			Next ();
		}

		if (route.Contains ("RELEASE")) {
			T.gameObject.SendMessage("Hold", false);
			Next ();
		}

		if (route.Contains ("FREEZE")) {
			T.gameObject.GetComponent<Rigidbody2D>().isKinematic = true;
			Next ();
		}

		if (route.Contains ("UNFREEZE")) {
			T.gameObject.GetComponent<Rigidbody2D>().isKinematic = false;
			Next ();
		}

		if (route.Contains ("RESIZE")) {
			float targetSize = float.Parse (route.Replace ("RESIZE TO ", null));
			float currentSize = T.GetComponent<Camera>().orthographicSize;
			if (Mathf.Abs (targetSize-currentSize) < .01f) {
				T.GetComponent<Camera>().orthographicSize = targetSize;
				Next ();
			}
			else {
				T.GetComponent<Camera>().orthographicSize = Mathf.Lerp (currentSize,
			                                                        targetSize, Time.deltaTime*Speed);
			}
		}

		if (route.Contains ("PLACE SIZE")) {
			float size = float.Parse (route.Replace ("PLACE SIZE AT ", null));
			T.GetComponent<Camera>().orthographicSize = size;
			Next ();
		}

		if (route == "CLEAR") {
			T.SendMessage ("Clear", true, SendMessageOptions.DontRequireReceiver);
			T.GetComponent<Collider2D>().enabled = false;
			Next ();
		}

		if (route.Contains ("UNCLEAR")) {
			T.SendMessage ("Clear", false, SendMessageOptions.DontRequireReceiver);
			T.GetComponent<Collider2D>().enabled = false;
			Next ();
		}

		if (route.Contains ("SET")) {
			T.gameObject.SendMessage ("SetBool", route.Replace ("SET ", null), SendMessageOptions.DontRequireReceiver);
			string[] parameters = route.Replace ("SET ", null).Split (' ');
			bool boolean = true;
			if (parameters[1] == "FALSE") boolean = false;

			if (T.GetComponent<Animator>() != null) {
				T.GetComponent<Animator>().SetBool(parameters[0], boolean);
			}

			Next();
		}

		if (route.Contains("FLIP RIGHT")) {
			T.transform.eulerAngles = new Vector3(0,180,0);
			Next ();
		}

		if (route.Contains("FLIP LEFT")) {
			T.transform.eulerAngles = new Vector3(0,0,0);
			Next ();
		}

		if (route.Contains("MOVE REL UP")) {
			Move (T,
			      T.transform.up,
			      float.Parse (route.Replace ("MOVE REL UP ", null)));
		}
		if (route.Contains("MOVE REL DOWN")) {
			Move (T,
			      -T.transform.up,
			      float.Parse (route.Replace ("MOVE REL DOWN ", null)));
		}
		if (route.Contains("MOVE REL LEFT")) {
			Move (T,
			      -T.transform.right,
			      float.Parse (route.Replace ("MOVE REL LEFT ", null)));
		}
		if (route.Contains("MOVE REL RIGHT")) {
			Move (T,
			      T.transform.right,
			      float.Parse (route.Replace ("MOVE REL RIGHT ", null)));
		}
		if (route.Contains("MOVE UP")) {
			Move (T,
			      Vector3.up,
			      float.Parse (route.Replace ("MOVE UP ", null)));
		}
		if (route.Contains("MOVE DOWN")) {
			Move (T,
			      Vector3.down,
			      float.Parse (route.Replace ("MOVE DOWN ", null)));
		}
		if (route.Contains("MOVE LEFT")) {
			Move (T,
			      Vector3.left,
			      float.Parse (route.Replace ("MOVE LEFT ", null)));
		}
		if (route.Contains ("MOVE RIGHT")) {
			Move (T,
			      Vector3.right,
			      float.Parse (route.Replace ("MOVE RIGHT ", null)));
		}
		if (route.Contains ("TURN")) {
			float angle = float.Parse (route.Replace ("TURN ", null));
			T.Rotate (Mathf.Sign (angle) * Vector3.forward * 50 * Speed * Time.deltaTime);
			if (Mathf.DeltaAngle (T.localEulerAngles.z, checkPoint.z+angle) < 3) {
				T.localEulerAngles = new Vector3(T.localEulerAngles.x,
				                                 T.localEulerAngles.y,
				                                      checkPoint.z+angle);
				Next ();
			}
		}
	}

	void Move (Transform T, Vector3 direction, float distance) {
		if (Flip) {
			if (direction.x > 0) T.localScale = new Vector3(1,1,1);
			if (direction.x < 0) T.localScale = new Vector3(-1,1,1);
		}
		Vector3 targetPos = checkPoint+direction*distance;
		if (Vector3.Distance (T.position, targetPos) < .001f) {
			T.position = targetPos;
			Next ();
		} else {
			T.position = Vector3.MoveTowards (T.position, targetPos, Speed / 50);
		}
		if (distance == Mathf.Infinity) {
			Next ();
		}

	}

	void Follow (Transform T) {
		RaycastHit2D[] pathHits =  Physics2D.CircleCastAll (transform.position, 200, Vector3.zero, 0, pathMask);
		CheckCircle (pathHits, T);
	}


	void CheckCircle(RaycastHit2D[] circleHits, Transform T) {
		for (int i = 0; i < circleHits.Length; i++) {
			Transform h = circleHits[i].transform;
			bool isMouse = false;
			if (h.tag == "Path" || h.tag == "Mouse") {
				Vector3 dir = h.position - T.position;
				dir = dir / dir.magnitude;
				RaycastHit2D hit = Physics2D.Raycast (T.position, dir);

				if (hit.collider && hit.transform == h) {
					if (h.tag == "Path") {
						PathManager manager = h.GetComponent<PathManager> ();
						for (int j = 0; j < manager.subPaths.Count; j++) {
							PathData data = manager.subPaths [j].currentData;
							if (data != null && data.Paths.Count > 0 && data.Paths [0].tag.ToUpper () == followTag.ToUpper ()) {
								data.Distance += Vector2.Distance (h.position, T.position);
								if (data.Distance <= currentData.Distance || data.Time > currentData.Time) {
									ResetFollow ();
									Debug.Log (h.position);
									currentData = data;

									string newRoute = "MOVE TO " + "(" + h.position.x + "," + h.position.y + ")";

									if (followMove != -1) {
										Route [followMove] = "FOLLOW " + followTag + " && " + newRoute;
									}
								}
							}
						}
					} else if (h.tag == "Mouse") {
						ResetFollow ();
						currentData.Paths.Add (h);
						currentData.Distance = 0;
						currentData.Time = Time.time;

						string newRoute = "MOVE TO " + "(" + h.position.x + "," + h.position.y + ")";

						if (followMove != -1) {
							
							Route [followMove] = "FOLLOW " + followTag + " && " + newRoute;
						}
						return;
					}
				}
			}
		}
	}

	void EndTime () {
		for (int i = 0; i < moves.Length; i++) {
			if (moves[i].Contains ("WAIT")) {
				if (moves[i].Contains ("TARGET")) {
					moves[i] = "TARGET " + targetNum + ": NULL";
				}
				else {
					moves[i] = "NULL";
				}

				Check ();
				break;
			}
		}
	}

	public void ResetFollow () {
		if (followMove != -1) Route [followMove] = "FOLLOW " + followTag;
		currentData = new PathData ();
		//Next ();
	}
	
	void Next () {
		if (!moves [index].Contains ("WAIT") && !moves [index].Contains ("INFINITY") && !moves [index].Contains ("KEY")) {
			if (moves[index].Contains ("TARGET")) {
				moves[index] = "TARGET " + targetNum + ": NULL";
			}
			else {
				moves[index] = "NULL";
			}
		}
		
		checkPoints[index] = new Vector3(100,100,100);
		Check ();
	}

	void Check () {
		for (int i = 0; i < moves.Length; i++) {
			if (moves [i] != "" && !moves[i].Contains ("INFINITY") && !moves[i].Contains ("KEY")
			    && !moves[i].Contains ("NULL")) {
				return;
			}
			if (moves[i] == "WAIT INFINITY") {
				return;
			}
		}
		nextMove = true;
		currentMove++;
	}
}